# Elevate privilege
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco feature enable -n useFipsCompliantChecksums

# Chocolatey packages
$pkgs = @(
  "git",
  "poshgit",
  "tortoisegit",
  "notepadplusplus",
  "vim",
  "installroot",
  "putty-cac",
  "openssh",
  "openssl",
  "curl",
  "wget",
  "winscp.install",
  "filezilla",
  "7zip",
  "python",
  "pyenv-win",
  "pycharm",
  "jdk8",
  "chefdk",
  "ruby",
  "nodejs",
  "nvm",
  "fiddler",
  "postman",
  "atom",
  "vscode",
  "vscode-python",
  "docker-desktop",
  "kubernetes-cli",
  "eclipse",
  "cloudfoundry-cli",
  "awscli",
  "anaconda3",
  "r",
  "r.studio",
  "pgadmin4",
  "postgresql12",
  "oracle-sql-developer",
  "sql-server-management-studio",
  "sql-server-odbcdriver",
  "libreoffice",
  "gimp",
  "drawio",
  "firefox",
  "googlechrome",
  "amazon-workspaces",
  "adobereader",
  "citrix-workspace",
  "mobaxterm",
  "xming",
  "terraform",
  "rocketchat",
  "qgis"
)

ForEach ($pkg in $pkgs) {
  choco install -y $pkg
}
