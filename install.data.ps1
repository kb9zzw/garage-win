# Elevate privilege
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco feature enable -n useFipsCompliantChecksums

# Chocolatey packages
$pkgs = @(
  "git",
  "poshgit",
  "tortoisegit",
  "notepadplusplus",
  "vim",
  "installroot",
  "putty-cac",
  "openssh",
  "openssl",
  "curl",
  "wget",
  "winscp.install",
  "filezilla",
  "7zip",
  "python",
  "pyenv-win",
  "pycharm",
  "atom",
  "vscode",
  "vscode-python",
  "cloudfoundry-cli",
  "awscli",
  "anaconda3",
  "r",
  "r.studio",
  "pgadmin4",
  "postgresql12",
  "sql-serever-management-studio",
  "sql-server-odbcdriver",
  "oracle-sql-developer",
  "drawio",
  "firefox",
  "googlechrome",
  "qgis"
)

ForEach ($pkg in $pkgs) {
  choco install -y $pkg
}
