# garage-win

Bootstraps Windows Development with Chocolatey!

## What is this?

This is a collection of PowerShell scripts that will install Windows 
applications using the Chocolatey package manager.

## Install

**Choose which baseline you want**

* install.full.ps1 = The kitchen sink.
* install.development.ps1 = Development tools
* install.data.ps1 = Data and data science tools
* install.minimal.ps1 = Just the basics

**Run the script**

Right-click on the script and "Run in PowerShell".  This will prompt you for
elevated privileges.  Click OK to continue.

**Wait**

It can take a while to install all this stuff, perhaps 30-60 minutes.   Be patient.  Go get a coffee.

## What does it install?

The package list will change from time to time. Look at each `.ps1` file.  The package list is 
an array of package titles from Chocolatey.

## But I want something else!

There are thousands of packages in Chocolatey.  Open a PowerShell window as Administrator and
search/install what you want.

```
# Search
choco search [package-name]

# Install
choco install [package-name]
```

More info is available on [Chocolatey](https://chocolatey.org).

## Related Projects

Looking for Mac or Linux?  Try my [garage](../../garage).
