# Elevate privilege
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco feature enable -n useFipsCompliantChecksums

# Chocolatey packages
$pkgs = @(
  "git",
  "poshgit",
  "notepadplusplus",
  "installroot",
  "putty-cac",
  "openssh",
  "openssl",
  "7zip"
)

ForEach ($pkg in $pkgs) {
  choco install -y $pkg
}
